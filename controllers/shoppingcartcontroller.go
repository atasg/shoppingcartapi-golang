package controllers

import (
	"context"
	"log"
	"time"
	"strconv"
	"math"
	"encoding/json"
	"shoppingcart/models"
	"shoppingcart/config"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"github.com/gofiber/fiber/v2/middleware/session"
)

var store = session.New()

func AddProduct(c *fiber.Ctx) error {
	collection := config.MI.DB.Collection("products")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	product := new(models.Product)

	if err := c.BodyParser(product); err != nil {
		log.Println(err)
		return c.Status(400).JSON(fiber.Map{
			"success": false,
			"message": "Failed to parse body",
			"error":   err,
		})
	}

	result, err := collection.InsertOne(ctx, product)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{
			"success": false,
			"message": "product failed to insert",
			"error":   err,
		})
	}
	return c.Status(fiber.StatusCreated).JSON(fiber.Map{
		"data":    result,
		"success": true,
		"message": "product inserted successfully",
	})
}

func GetAllProducts(c *fiber.Ctx) error {
	collection := config.MI.DB.Collection("products")
    ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	sess, err := store.Get(c)
    if err != nil {
            panic(err)
    }

	strCart,_ := sess.Get("cart").(string)
	

	var cartToSend []models.Item
	json.Unmarshal([]byte(strCart),&cartToSend)



    var products []models.Product

	filter := bson.M{}
    findOptions := options.Find()
    //  do not forget 
    if s := c.Query("s"); s != "" {
        filter = bson.M{
            "$or": []bson.M{
                {
                    "movieName": bson.M{
                        "$regex": primitive.Regex{
                            Pattern: s,
                            Options: "i",
                        },
                    },
                },
                {
                    "catchphrase": bson.M{
                        "$regex": primitive.Regex{
                            Pattern: s,
                            Options: "i",
                        },
                    },
                },
            },
        }
    }

    page, _ := strconv.Atoi(c.Query("page", "1"))
    limitVal, _ := strconv.Atoi(c.Query("limit", "10"))
    var limit int64 = int64(limitVal)

    total, _ := collection.CountDocuments(ctx, filter)

    findOptions.SetSkip((int64(page) - 1) * limit)
    findOptions.SetLimit(limit)

    cursor, err := collection.Find(ctx, filter, findOptions)
    defer cursor.Close(ctx)

    if err != nil {
        return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
            "success": false,
            "message": "Products Not found",
            "error":   err,
        })
    }

    for cursor.Next(ctx) {
        var product models.Product
        cursor.Decode(&product)
        products = append(products, product)
    }

    last := math.Ceil(float64(total / limit))
    if last < 1 && total > 0 {
        last = 1
    }

    return c.Status(fiber.StatusOK).JSON(fiber.Map{
        "data":      products,
        "total":     total,
        "page":      page,
        "last_page": last,
        "limit":     limit,
		"cart": cartToSend, 
    })
}

func AddProductToCart(c *fiber.Ctx) error{
	collection := config.MI.DB.Collection("products")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	objId, err := primitive.ObjectIDFromHex(c.Params("id"))
    if err != nil {
        return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
            "success": false,
            "message": "Product not found",
            "error":   err,
        })
    }
	result := collection.FindOne(ctx, bson.M{"_id": objId})
	var product models.Product
	result.Decode(&product)

	sess, err := store.Get(c)
    if err != nil {
        panic(err)
    }

	// get value 
	cart := sess.Get("cart")
	var cartToAdd []models.Item
	if cart == nil {
		cartToAdd = append(cartToAdd, models.Item{
			Product: product,
			Quantity: 1,
		})
		bytesCart,_ := json.Marshal(cartToAdd)
		// set key and value
		sess.Set("cart", string(bytesCart))
		// Save session
		if err := sess.Save(); err != nil {
			panic(err)
		}
		
	} else {
		cart,_ := sess.Get("cart").(string)
		json.Unmarshal([]byte(cart),&cartToAdd)

		// check if the id exists

		added := false
		for i:=0; i<len(cartToAdd); i++ {
			// if exists, increase the quantity
			if cartToAdd[i].Product == product{
				cartToAdd[i].Quantity = cartToAdd[i].Quantity + 1
				added = true
				bytesCart,_ := json.Marshal(cartToAdd)
				// set key and value
				sess.Set("cart", string(bytesCart))
				// Save session
				if err := sess.Save(); err != nil {
					panic(err)
				}
			}
			
		}
		// if not exist, append the product to slice
		if !added {
			cartToAdd = append(cartToAdd, models.Item{
				Product: product,
				Quantity: 1,
			})
			bytesCart,_ := json.Marshal(cartToAdd)
			// set key and value
			sess.Set("cart", string(bytesCart))
			// Save session
			if err := sess.Save(); err != nil {
				panic(err)
			}
		}

	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
        "cart": cartToAdd,
    })
}

func RemoveProductFromCart(c *fiber.Ctx) error{
	collection := config.MI.DB.Collection("products")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	objId, err := primitive.ObjectIDFromHex(c.Params("id"))
    if err != nil {
        return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
            "success": false,
            "message": "Product not found",
            "error":   err,
        })
    }
	result := collection.FindOne(ctx, bson.M{"_id": objId})
	var product models.Product
	result.Decode(&product)

	sess, err := store.Get(c)
    if err != nil {
        panic(err)
    }

	// get value 
	cart := sess.Get("cart")
	var cartToRemove []models.Item
	if cart != nil {
		cart,_ := sess.Get("cart").(string)
		json.Unmarshal([]byte(cart),&cartToRemove)

		// check if the id exists
		for i:=0; i<len(cartToRemove); i++ {
			// if exists, increase the quantity
			if cartToRemove[i].Product == product{
				if cartToRemove[i].Quantity > 1 {
					cartToRemove[i].Quantity = cartToRemove[i].Quantity - 1
					bytesCart,_ := json.Marshal(cartToRemove)
					// set key and value
					sess.Set("cart", string(bytesCart))
					// Save session
					if err := sess.Save(); err != nil {
						panic(err)
					}

				} else {
					// Remove the element at index i from a.
					cartToRemove[i] = cartToRemove[len(cartToRemove)-1] // Copy last element to index i.
					cartToRemove = cartToRemove[:len(cartToRemove)-1]   // Truncate slice.
					bytesCart,_ := json.Marshal(cartToRemove)
					// set key and value
					sess.Set("cart", string(bytesCart))
					// Save session
					if err := sess.Save(); err != nil {
						panic(err)
					}
				}
				
			}
			
		}
		
	} 

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
        "cart": cartToRemove,
    })
}

func GetCart(c *fiber.Ctx) error {
	sess, err := store.Get(c)
    if err != nil {
        panic(err)
    }

	cart, _ := sess.Get("cart").(string)
	var cartToSend []models.Item
	json.Unmarshal([]byte(cart),&cartToSend)

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"cart": cartToSend,
	})

}

func DeleteCart(c *fiber.Ctx) error{
	sess, err := store.Get(c)
    if err != nil {
        panic(err)
    }
	sess.Delete("cart")
	// Save session
    if err := sess.Save(); err != nil {
        panic(err)
    }

	cart, _ := sess.Get("cart").(string)
	var cartToSend []models.Item
	json.Unmarshal([]byte(cart),&cartToSend)

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"cart": cartToSend,
		"message": "cart successfully deleted",
	})
}


