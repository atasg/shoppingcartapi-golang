package main

import (
	"shoppingcart/config"
	"shoppingcart/routes"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func hello(c *fiber.Ctx) error {
	return c.SendString("Hello this is a first try!")

}

func main() {

	app := fiber.New()

	app.Use(cors.New(cors.Config{
		AllowOrigins: "http://127.0.0.1:3000",
		AllowHeaders: "*",
		AllowCredentials: true,
	}))
	app.Get("/", hello)
	config.ConnectDB()
	routes.RoutersShoppingCart(app)
	app.Listen(":8080")
}
