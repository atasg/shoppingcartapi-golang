package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Product struct {
	ID    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name  string             `json:"name,omitempty" bson:"name,omitempty"`
	Price float64            `json:"price,omitempty" bson:"price,omitempty"`
	Desc  string             `json:"desc,omitempty" bson:"desc,omitempty"`
	Rate  float64            `json:"rate,omitempty" bson:"rate,omitempty"`
}
type Item struct {
	Product  Product `json:"items,omitempty" bson:"items,omitempty"`
	Quantity int64   `json:"quantity,omitempty" bson:"quantity,omitempty"`
}
