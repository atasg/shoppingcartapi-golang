package routes

import (
    "github.com/gofiber/fiber/v2"
	"shoppingcart/controllers"
)

func RoutersShoppingCart(app *fiber.App) {
	app.Post("shoppingcart/addproduct", controllers.AddProduct)
	app.Get("shoppingcart/allproducts",controllers.GetAllProducts)
	app.Get("shoppingcart/addproducttocart/id::id", controllers.AddProductToCart)
	app.Get("shoppingcart/removeproductfromcart/id::id", controllers.RemoveProductFromCart)
	app.Get("shoppingcart/getcart", controllers.GetCart)
	app.Get("shoppingcart/deletecart",controllers.DeleteCart)
}